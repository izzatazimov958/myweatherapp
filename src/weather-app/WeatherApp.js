import React, { useEffect, useState, useCallback } from "react";
import "./weatherapp.scss";
import axios from "axios";

const WeatherApp = () => {
  const [weather, setWeather] = useState([]);
  const [dateOutput, setDateOutput] = useState("");
  const [input, setInput] = useState("london");
  const [conditionText, setConditionText] = useState("");

  const { location } = weather;
  const { current } = weather;
  console.log(weather);
  const date = location?.localtime;
  const is_day = current?.is_day;

  let timeOfDay = "day";

  if (!is_day) {
    timeOfDay = "night";
  }

  const y = parseInt(date?.substr(0, 4));

  const m =
    parseInt(date?.substr(5, 2)) < 10
      ? `${date?.substr(5, 2)}`
      : date?.substr(5, 2);

  const d = parseInt(date?.substr(8, 2));
  const time = date?.substr(11);

  const getWeather = useCallback(
    (param) => {
      axios
        .get(
          `https://api.weatherapi.com/v1/current.json?key=fb8b4e38384f4bedbc2145430222104&q=${
            param ? param : "London"
          }`
        )
        .then((res) => setWeather(res.data));
      setConditionText(current?.condition?.text.toLowerCase());
    },
    [current?.condition?.text]
  );

  const clickedLi = (e) => {
    setInput(e.target.innerText);

    getWeather(e.target.innerText);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    getWeather(input);
    console.log(input);
  };

  const dayOfTheWeek = (day, month, year) => {
    const weekDay = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];
    return weekDay[new Date(year, month - 1, day).getDay()];
  };

  useEffect(() => {
    getWeather(input);
    dayOfTheWeek(d, m, y);
    setDateOutput(dayOfTheWeek(d, m, y) + " " + d + " " + m + " " + y);
  }, [getWeather, input, d, m, y]);

  return (
    <div>
      <div
        className="weather-app"
        style={{
          backgroundImage: `url(${require(`../images/${timeOfDay}/${
            conditionText ? "rainy" : "clear"
          }.jpg`)})`,
        }}
      >
        <div className="container">
          <h3 className="brand">the weather</h3>
          <div className="content">
            <h1 className="temp">{current?.temp_c}&#176;</h1>
            <div className="city-time">
              <h1 className="name">{location?.name}</h1>
              <small>
                <span className="time">{time && time}</span>-
                <span className="date">
                  {" "}
                  {dateOutput ? dateOutput : "null"}
                </span>
              </small>
            </div>
            <div className="weather">
              <img
                src={
                  current?.condition?.icon
                    ? current?.condition?.icon
                    : require("../icons/day/113.png")
                }
                alt="icon"
                className="icon"
                width="50"
                height="50"
              />
              <span className="condition"> {current?.condition?.text}</span>
            </div>
          </div>
        </div>
        <div className="panel">
          <form id="locationInput" onSubmit={handleSubmit}>
            <input
              type="text"
              className="search"
              placeholder="Another location"
              value={input}
              onChange={(e) => setInput(e.target.value)}
            />
            <button type="submit" className="submit">
              <i className="fas fa-search"></i>
            </button>
          </form>
          <ul className="cities">
            <li className="city" onClick={(e) => clickedLi(e)} value="Tashkent">
              Tashkent
            </li>
            <li
              className="city"
              onClick={(e) => clickedLi(e)}
              value="Samarkand"
            >
              Samarkand
            </li>
            <li className="city" onClick={(e) => clickedLi(e)} value="Andijan">
              Andijan
            </li>
            <li className="city" onClick={(e) => clickedLi(e)} value="Fergana">
              Fergana
            </li>
          </ul>

          <ul className="details">
            <h4>Weather Details</h4>
            <li>
              <span>Feels like</span>
              <span className="cloud">{current?.feelslike_c}&#176;</span>

            </li>
            <li>
              <span>Cloudy</span>
              <span className="cloud">{current?.cloud}%</span>
            </li>
            <li>
              <span>Humidity</span>
              <span className="humidity">{current?.humidity}%</span>
            </li>
            <li>
              <span>Wind</span>
              <span className="wind">{current?.wind_kph}km/h</span>
            </li>
            <li>
              <span>UV</span>
              <span className="wind">{current?.uv}</span>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default WeatherApp;
