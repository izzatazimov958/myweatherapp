import './App.scss';
import WeatherApp from './weather-app/WeatherApp';


function App() {
  return (
    <div className="app">
      <WeatherApp />
    </div>
  );
}

export default App;
